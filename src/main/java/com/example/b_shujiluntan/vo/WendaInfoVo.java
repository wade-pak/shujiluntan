package com.example.b_shujiluntan.vo;

import com.example.b_shujiluntan.entity.WendaInfo;

import java.util.List;

public class WendaInfoVo extends WendaInfo {
    private List<WendaInfoVo> children;

    public List<WendaInfoVo> getChildren() {
        return children;
    }

    public void setChildren(List<WendaInfoVo> children) {
        this.children = children;
    }
}
