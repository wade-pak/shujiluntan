package com.example.b_shujiluntan.vo;

import com.example.b_shujiluntan.entity.DianzishuInfoComment;

import java.util.List;

public class DianzishuInfoCommentVo extends DianzishuInfoComment {

    private String foreignName;

    private List<DianzishuInfoCommentVo> children;

    public List<DianzishuInfoCommentVo> getChildren() {
        return children;
    }

    public void setChildren(List<DianzishuInfoCommentVo> children) {
        this.children = children;
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName;
    }
}