package com.example.b_shujiluntan.controller;

import com.example.b_shujiluntan.common.Result;
import com.example.b_shujiluntan.entity.ShitishuInfo;
import com.example.b_shujiluntan.service.*;
import com.example.b_shujiluntan.vo.ShitishuInfoVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/shitishuInfo")
public class ShitishuInfoController {
    @Resource
    private ShitishuInfoService shitishuInfoService;

    @PostMapping
    public Result<ShitishuInfo> add(@RequestBody ShitishuInfoVo info) {
        shitishuInfoService.add(info);
        return Result.success(info);
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id) {
        shitishuInfoService.delete(id);
        return Result.success();
    }

    @PutMapping
    public Result update(@RequestBody ShitishuInfoVo info) {
        shitishuInfoService.update(info);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<ShitishuInfo> detail(@PathVariable Long id) {
        ShitishuInfo info = shitishuInfoService.findById(id);
        return Result.success(info);
    }

    @GetMapping
    public Result<List<ShitishuInfoVo>> all() {
        return Result.success(shitishuInfoService.findAll());
    }

    @GetMapping("/page/{name}")
    public Result<PageInfo<ShitishuInfoVo>> page(@PathVariable String name,
                                                @RequestParam(defaultValue = "1") Integer pageNum,
                                                @RequestParam(defaultValue = "5") Integer pageSize,
                                                HttpServletRequest request) {
        return Result.success(shitishuInfoService.findPage(name, pageNum, pageSize, request));
    }
}
