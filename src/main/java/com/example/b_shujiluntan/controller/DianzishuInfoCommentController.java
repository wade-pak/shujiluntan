package com.example.b_shujiluntan.controller;

import com.example.b_shujiluntan.common.Result;
import com.example.b_shujiluntan.entity.DianzishuInfoComment;
import com.example.b_shujiluntan.service.DianzishuInfoCommentService;
import com.example.b_shujiluntan.vo.DianzishuInfoCommentVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/dianzishuInfoComment")
public class DianzishuInfoCommentController {
    @Resource
    private DianzishuInfoCommentService dianzishuInfoCommentService;

    @PostMapping
    public Result<DianzishuInfoComment> add(@RequestBody DianzishuInfoComment commentInfo, HttpServletRequest request) {
        dianzishuInfoCommentService.add(commentInfo, request);
        return Result.success(commentInfo);
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id) {
        dianzishuInfoCommentService.delete(id);
        return Result.success();
    }

    @PutMapping
    public Result update(@RequestBody DianzishuInfoComment commentInfo) {
        dianzishuInfoCommentService.update(commentInfo);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<DianzishuInfoComment> detail(@PathVariable Long id) {
        DianzishuInfoComment commentInfo = dianzishuInfoCommentService.findById(id);
        return Result.success(commentInfo);
    }

    @GetMapping
    public Result<List<DianzishuInfoCommentVo>> all() {
        return Result.success(dianzishuInfoCommentService.findAll());
    }

    @GetMapping("/page/{name}")
    public Result<PageInfo<DianzishuInfoCommentVo>> page(@PathVariable String name,
                                                @RequestParam(defaultValue = "1") Integer pageNum,
                                                @RequestParam(defaultValue = "5") Integer pageSize,
                                                HttpServletRequest request) {
        return Result.success(dianzishuInfoCommentService.findPage(name, pageNum, pageSize));
    }

    @GetMapping("/findByForeignId/{id}")
    public Result<List<DianzishuInfoCommentVo>> findByForeignId (@PathVariable Long id) {
        return Result.success(dianzishuInfoCommentService.findByForeignId(id));
    }
}
