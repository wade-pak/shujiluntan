package com.example.b_shujiluntan.dao;

import com.example.b_shujiluntan.entity.AdvertiserInfo;
import com.example.b_shujiluntan.vo.AdvertiserInfoVo;


import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface AdvertiserInfoDao extends Mapper<AdvertiserInfo> {
    List<AdvertiserInfoVo> findByName(@Param("name") String name);
}
