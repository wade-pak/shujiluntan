package com.example.b_shujiluntan.dao;

import com.example.b_shujiluntan.entity.DianzishuInfo;
import com.example.b_shujiluntan.vo.DianzishuInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface DianzishuInfoDao extends Mapper<DianzishuInfo> {
    List<DianzishuInfoVo> findByNameAndId(@Param("name") String name, @Param("id") Long id);
}