package com.example.b_shujiluntan.dao;

import com.example.b_shujiluntan.entity.ShitishuInfo;
import com.example.b_shujiluntan.vo.ShitishuInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ShitishuInfoDao extends Mapper<ShitishuInfo> {
    List<ShitishuInfoVo> findByName(@Param("name") String name);



}
