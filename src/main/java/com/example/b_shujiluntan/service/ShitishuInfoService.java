package com.example.b_shujiluntan.service;

import com.example.b_shujiluntan.dao.ShitishuInfoDao;
import com.example.b_shujiluntan.entity.ShitishuInfo;
import com.example.b_shujiluntan.vo.ShitishuInfoVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class ShitishuInfoService {

    @Resource
    private ShitishuInfoDao shitishuInfoDao;

    public ShitishuInfo add(ShitishuInfo info) {
        shitishuInfoDao.insertSelective(info);
        return info;
    }

    public void delete(Long id) {
        shitishuInfoDao.deleteByPrimaryKey(id);
    }

    public void update(ShitishuInfo info) {
        shitishuInfoDao.updateByPrimaryKeySelective(info);
    }

    public ShitishuInfo findById(Long id) {
        return shitishuInfoDao.selectByPrimaryKey(id);
    }

    public List<ShitishuInfoVo> findAll() {
        return shitishuInfoDao.findByName("all");
    }

    public PageInfo<ShitishuInfoVo> findPage(String name, Integer pageNum, Integer pageSize, HttpServletRequest request) {
        PageHelper.startPage(pageNum, pageSize);
        List<ShitishuInfoVo> all = findAllPage(request, name);
        return PageInfo.of(all);
    }

    public List<ShitishuInfoVo> findAllPage(HttpServletRequest request, String name) {
        return shitishuInfoDao.findByName(name);
    }

}