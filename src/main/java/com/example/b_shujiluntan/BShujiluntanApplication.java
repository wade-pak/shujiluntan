package com.example.b_shujiluntan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.b_shujiluntan.dao")
public class BShujiluntanApplication {

  public static void main(String[] args) {
        SpringApplication.run(BShujiluntanApplication.class, args);
    }

}
