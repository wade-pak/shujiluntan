# 书籍论坛系统

#### 介绍
{**以下是书籍论坛系统介绍**
书籍论坛系统是我们小组4人模仿别人的项目来进行开发实验的，适合初学者对后端框架研究，前端代码由于涉及vue，jquery，JavaScript等知识点众多，我们旨在培养Spring boot框架的各层次思想。即使在对别人的项目进行逆向开发，我们也会遇到不少难题，项目不完善之处，还请海涵。项目请看 [传送门](https://gitee.com/wade-pak/shujiluntan.git)}

#### 软件架构  

*前端*  
- HTML,CSS,JavaScript  
- bootstrap布局  
- Vue.js数据渲染  
- echarts统计图  
- Quill富文本  
  
*后端*  
- Springboot框架  
- Mybatis持久层  
- MySQL数据库  
- 自定义的权限功能  


#### 安装教程

1.  下载Navicat Premium  
2.  下载mysql5.7并连接  
3.  git clone到自己本地用Intellij Idea打开  

#### 使用说明

1.  在idea中打开刚刚git clone到本地的文件夹
2.  配置我们的application.yml以连接数据库  
![63.png](https://s2.loli.net/2021/12/12/WnfdMAL5NC23TJt.png)  
3.  在数据库中导入我们的sql脚本以建表  
![62.png](https://s2.loli.net/2021/12/12/iyDLhvIKg13TaNu.png)
4.  刷新一下数据库内容，可以看到刚刚建好的表  
![64.png](https://s2.loli.net/2021/12/12/YnD8cZ5RIXdh4Ob.png)  
5.  打开localhost：配置的端口（8088）+ /end/page/login.html  
6.  登录界面管理员admin 密码123456
7.  注册界面可选择自己想要的内容进行注册
8.  注册后可用刚刚注册的账号进行登录  

#### 参与贡献

1.  gkd的各位同学们也可用成为我们的一员，共同将项目完善
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request


#### 运行截图

1.  登录进去后的界面  
![65.png](https://s2.loli.net/2021/12/12/b2lzOjIePsNvB74.png)  
2.  在后台的增删改查对数据库同等生效  
![66.png](https://s2.loli.net/2021/12/12/FdnrO2qkUK63S8P.png)  
![67.png](https://s2.loli.net/2021/12/12/BdJTIjaofNEbu59.png)


```
这里输入代码
```
